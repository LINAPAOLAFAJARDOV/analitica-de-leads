return 
truncate table [BodegaCorporativa].[bodega].[factLeads_2022_3mesesV2]

select [dtmFechaInsercion]
      ,MIN([dtmFechaActualizacion]) [dtmFechaActualizacion]
	  ,MAX([dtmFechaActualizacion]) [dtmFechaActualizacion]
	  FROM [BodegaCorporativa].[bodega].[factLeads_2022_3mesesV2]
	  GROUP BY [dtmFechaInsercion]


select [dtmFechaInsercion]
      ,MIN([dtmFechaActualizacion]) [dtmFechaActualizacion]
	  ,MAX([dtmFechaActualizacion]) [dtmFechaActualizacion]
	  FROM [BodegaCorporativa].[bodega].[factLeads]
	  GROUP BY [dtmFechaInsercion] order by 1

INSERT INTO [BodegaCorporativa].[bodega].[factLeads_2022_3mesesV2]
SELECT [skTiempo]
      ,[skCampaña]
      ,[skRangoCanalLeads]
      ,[strIdentificacion]
      ,[strFuente]
      ,[strPeriodo]
      ,[strDescCanalLeads]
      ,[indRegistroValido]
      ,[indVinculadoNuevo]
      ,[dtmFechaVinculacion]
      ,[dtmFechaRegistro]
      ,[dtmFechaInsercion]
      ,[dtmFechaActualizacion]
      ,[skAsesor]
      ,[strTipoLead]
      ,[indRegistroDepurado]
      ,[IndCargadoCallCenter]
      ,[IndCalentamiento]
      ,[IndGestion]
      ,[IndContactado]
      ,[IndContactoDirecto]
      ,[indEfectivo]
      ,[indPreingresos]
      ,[strCanalCallCenter]
      ,[IdentificacionAgenteCallCenter]
      ,[AgenteCallCenter]
      ,[dtmFechaGestionCallCenter]
      ,[dtmFechaCargueCallCenter]
      ,[dtmFechaCalentamiento]
      ,[skMotivoNoContacto]
      ,[skMotivoNoEfectivo]
      ,[strDescTipoCanalLeads]
      ,[skRangoCanalCallCenter]
      ,[strBaseCallCenter]
      ,[skRangoFuenteLeads]
      ,[dtmFechaEfectivoCallCenter]
      ,[AgenteCallCenterEfectivo]
      ,[strBaseCallCenterEfectivo]
      ,[skMotivoGestionContacto]
      ,[skMotivoEfectivo]
      ,[strCanalCallCenterEfectivo]
      ,[dtmFechaActualizacionPreIngresos]
      ,[indVinculadoNuevoSinCono]
      ,[indPreingresosSinCono]
  FROM [BodegaCorporativa].[bodega].[factLeads]
  where $partition.pf_mes(dtmFechaInsercion) between $partition.pf_mes('2022-01-01') and $partition.pf_mes('2022-06-01')

  DELETE
  FROM [BodegaCorporativa].[bodega].[factLeads]
  where $partition.pf_mes(dtmFechaInsercion) between $partition.pf_mes('2022-01-01') and $partition.pf_mes('2022-02-01')


  INSERT INTO [BodegaCorporativa].[bodega].[factLeads]
  SELECT * FROM [BodegaCorporativa].[bodega].[factLeads_20221018]
  where $partition.pf_mes(dtmFechaInsercion) between $partition.pf_mes('2022-01-01') and $partition.pf_mes('2022-02-01')
  