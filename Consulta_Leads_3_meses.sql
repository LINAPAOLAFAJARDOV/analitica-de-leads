IF OBJECT_ID('tempdb.dbo.#extrPlanoCallCenterCargados') IS NOT NULL DROP TABLE #extrPlanoCallCenterCargados

SELECT CEDULA,Periodo,  CAST(MAX([Fecha de cargue]) AS DATE) [Fecha de cargue] 
	into #extrPlanoCallCenterCargados
FROM StageCorporativa.stage.extrPlanoCallCenterCargados O
--WHERE Periodo BETWEEN  '202201' AND '202206' AND IndEncontrado = 0 
WHERE Periodo >= '202201' 
--AND EXISTS (SELECT 1 FROM [BodegaCorporativa].[bodega].[factLeads_2022_3meses] A WHERE a.strIdentificacion = O.CEDULA )
GROUP BY CEDULA, Periodo


CREATE INDEX idx_Cedula
ON #extrPlanoCallCenterCargados (CEDULA);

CREATE INDEX idx_Periodo
ON #extrPlanoCallCenterCargados (Periodo);

delete from [BodegaCorporativa].[bodega].[factLeads_2022_3meses_CompletaV2]

insert into [BodegaCorporativa].[bodega].[factLeads_2022_3meses_CompletaV2]
SELECT  A.*,
CASE strDescCategoria
    WHEN 'Adm�n. Regional - Ejecutivo Comercial'  THEN 'Regional'
    WHEN 'Adm�n. Regional - Ejecutivo Integral' THEN 'Regional'
    WHEN 'Adm�n. Regional - Asesor Integral' THEN 'Regional'
    WHEN 'Adm�n. Css - C�lula Nacional' THEN 'CSS'
    WHEN 'Adm�n. Css - Ejecutivo Comercial' THEN 'CSS'
    WHEN 'Adm�n. Css - Ejecutivo Integral' THEN 'CSS'
    WHEN 'Otros Canales' THEN 'Otros Canales'
    WHEN 'Asociado Cooperador' THEN 'Otros Canales'
    WHEN 'Corredores' THEN 'Otros Canales'
    WHEN 'Administrativo' THEN 'Otros Canales'
    WHEN 'Interno' THEN 'Otros Canales'
    WHEN 'Gestor Financiero' THEN 'Otros Canales'
    ELSE 'Otros Canales' --strDescCategoria
    END strDescCategoriaAgrupacion,
CASE strDescCategoria
    WHEN 'Adm�n. Regional - Ejecutivo Comercial'  THEN 'Ejecutivo Comercial'
    WHEN 'Adm�n. Regional - Ejecutivo Integral' THEN 'Ejecutivo Integral'
    WHEN 'Adm�n. Regional - Asesor Integral' THEN 'Asesor Integral'
    WHEN 'Adm�n. Css - C�lula Nacional' THEN 'C�lula Nacional'
    WHEN 'Adm�n. Css - Ejecutivo Comercial' THEN 'Ejecutivo Comercial'
    WHEN 'Adm�n. Css - Ejecutivo Integral' THEN 'Ejecutivo Integral'
    ELSE IIF(strDescCargo like '%Gestores%financ%','Gestor Financiero',strDescCategoria)
    END strDescCategoriaVisualizar
	,NULL [CantidadInteracciones]
FROM (
SELECT A.[skTiempo]
      ,A.strIdentificacion
      ,C.numAnio
      ,C.strDescMes
      ,A.[skCampa�a]
      ,ISNULL(D.strAgrupacionCampana, 'Sin agrupaci�n') strAgrupacionCampana
      ,D.strDescCampana
      ,ISNULL(D.strDescCampanaHomologado, 'Sin agrupaci�n') strDescCampanaHomologado
      ,A.skRangoCanalLeads
      --,L.strDescRango DescRangoCanalLeads
      ,A.skRangoFuenteLeads
      ,E.strDescRango FuenteLeads
      ,A.strDescCanalLeads
      ,A.indRegistroValido
      ,A.indVinculadoNuevo
      ,A.indRegistroDepurado
      ,A.IndCargadoCallCenter
      ,A.IndCalentamiento
      ,A.IndGestion
      ,A.IndContactado
      ,A.IndContactoDirecto
      ,A.indEfectivo
      ,A.indPreingresos
      ,A.indVinculadoNuevoSinCono
      ,A.indPreingresosSinCono
      ,A.dtmFechaVinculacion
      ,A.dtmFechaRegistro
      ,A.strTipoLead
      ,A.strCanalCallCenter
      ,A.dtmFechaGestionCallCenter dtmFechaGestionCallCenter
      ,A.dtmFechaCargueCallCenter dtmFechaCargueCallCenter
      ,o.[Fecha de cargue]
      ,A.dtmFechaCalentamiento
      ,A.strDescTipoCanalLeads
      ,A.strBaseCallCenter
      ,A.dtmFechaEfectivoCallCenter
      ,A.strBaseCallCenterEfectivo
      ,A.AgenteCallCenterEfectivo
      ,CASE E.strDescRango
        WHEN 'Campa�as Digitales' THEN '1'
        WHEN 'Referidos de Asociados' THEN '2'
        WHEN 'Referidos Fuerza Comercial' THEN '3'
        WHEN 'Referidos Call Center' THEN '4'
        WHEN 'Bases de datos adquiridas' THEN '5'
        WHEN 'Clientes del GECC' THEN '6'
        ELSE '7'
        END + '. ' +
        CASE E.strDescRango
            WHEN 'Referidos de Asociados' THEN 'Referidos - Gesti�n de mercadeo'
            WHEN 'Referidos Fuerza Comercial' THEN 'Referidos - Gesti�n fuerza comercial'
            else  E.strDescRango
            END FuenteLeadsVisualizar
      ,A.[strFuente]
      --,A.[skIndicador]
      --,B.strDescIndicador
      ,A.[strPeriodo]
      --,A.[strCodIndicador]
      --,A.[numValorAcumMes]
      ,ISNULL(lkpAse.strDescCargo,F.strDescCargo) strDescCargo
      ,ISNULL(lkpAse.strNombreAsesor, F.strNombreAsesor) strNombreAsesor
      ,ISNULL(lkpAse.strNombreJefe, F.strNombreJefe) strNombreJefe
      ,ISNULL(lkpAse.strDescTipoAsesor, f.strDescTipoAsesor) strDescTipoAsesor
      --,IIF(F.strDescCategoria = 'Campo Nulo','Sin Canal',F.strDescCategoria) strDescCategoria
      ,IIF(ISNULL(lkpAse.strDescCargo,F.strDescCargo) LIKE '%Gestores%financ%','Gestor Financiero',
        IIF(ISNULL(lkpAse.strDescTipoAsesor, f.strDescTipoAsesor) = 'Campo Nulo','Otros Canales',
        IIF(ISNULL(lkpAse.strDescTipoAsesor, f.strDescTipoAsesor) = 'Red Cooperamos','Asociado Cooperador',    
            ISNULL(lkpAse.strDescTipoAsesor, f.strDescTipoAsesor)))) strDescCategoria
      ,ISNULL(lkpAse.strDescEstado, f.strDescEstado) strDescEstado
      ,J.strNombreGerenteZona
      ,IIF(G.strDescRango ='No Cruza','Sin informaci�n',G.strDescRango) CanalCallCenter
      ,H.strDescAgrupacion DescMotivoNoContacto
      ,H.strDescTipo AgrupacionMotivoNoContacto
      ,I.strDescAgrupacion DescMotivoNoEfectivo
      ,I.strDescTipo AgrupacionMotivoNoEfectivo
      ,M.strDescAgrupacion DescMotivoGestionContanto
      ,M.strDescTipo AgrupacionMotivoGestionContanto
      ,M.strDescAgrupacion DescMotivoEfectivo
      ,M.strDescTipo AgrupacionMotivoEfectivo
      ,A.strCanalCallCenterEfectivo
      ,K.strDescAgrupacion DescMotivoPendienteContacto
      ,K.strDescTipo AgrupacionMotivoPendienteContacto
      ,case E.strDescRango when 'Campa�as Digitales' then 1
                           when 'Referidos de Asociados' then 2
                           when 'Referidos Fuerza Comercial' then 3
                           when 'Referidos Call Center' then 4
                           when 'Bases de datos adquiridas' then 5
                           when 'Clientes del GECC' then 6
                           when 'Mercado Natural Fuerza Comercial' then 7
                           else -1 end ordenFuente
      ,CAST(A.skAsesor AS varchar) skAsesor 
  FROM [BodegaCorporativa].[bodega].[factLeads_2022_3mesesV2] A 
  INNER JOIN [BodegaCorporativa].[bodega].[dimTiempo] C ON A.skTiempo = C.skTiempo 
  INNER JOIN [BodegaCorporativa].[bodega].[dimCampa�aLeads] D ON A.skCampa�a = D.skCampa�a
  INNER JOIN [BodegaCorporativa].[bodega].[dimRango] E ON A.skRangoFuenteLeads = E.skRango and E.strDescTipoRango = 'Fuente Leads'
  LEFT JOIN [BodegaCorporativa].[bodega].[dimAsesor] F ON A.skAsesor = F.skAsesor and f.strFuente = 'Lico'
  LEFT JOIN BodegaCorporativa.Bodega.lkpAsesor Ase ON A.skAsesor = Ase.[numIdAsesor]
        AND A.dtmFechaVinculacion BETWEEN Ase.dtmFechaInicio AND Ase.dtmFechaFin
  LEFT JOIN [BodegaCorporativa].[bodega].[lkpAsesor] lkpAse ON Ase.nkIdAsesor = lkpAse.nkIdAsesor and lkpAse.strFuente = 'Lico'
  LEFT JOIN BodegaCorporativa.bodega.dimRango G on A.skRangoCanalCallCenter = G.skRango
  LEFT JOIN BodegaCorporativa.bodega.dimMotivo H on A.skMotivoNoContacto = H.skMotivo and H.strDescTipo in ('No Contacto','No Contacto - Indirecto')
  LEFT JOIN BodegaCorporativa.bodega.dimMotivo I on A.skMotivoNoEfectivo = I.skMotivo and I.strDescTipo in ('No Cumple Perfil','No interesado')
  LEFT JOIN BodegaCorporativa.bodega.dimMotivo M on A.skMotivoGestionContacto = M.skMotivo
  LEFT JOIN BodegaCorporativa.bodega.dimMotivo N on A.skMotivoEfectivo = N.skMotivo
  LEFT JOIN (SELECT strCodOficina, MAX(strNombreGerenteZona) strNombreGerenteZona FROM BodegaCorporativa.bodega.dimGeografiaGecc
                WHERE strFuente = 'CooTaylor' and indRegistroActual = 1 GROUP BY strCodOficina) J on F.strCodOficina = J.strCodOficina
  LEFT JOIN BodegaCorporativa.bodega.dimMotivo K on a.skMotivoNoContacto = K.skMotivo and K.strDescTipo = 'Contacto Pendiente'
  --LEFT JOIN [BodegaCorporativa].[bodega].[dimRango] L ON A.skRangoCanalLeads = L.skRango and L.strDescTipoRango = 'Canal Leads'
  LEFT JOIN #extrPlanoCallCenterCargados 
	/*(SELECT CEDULA,Periodo, IndEncontrado, MAX([Fecha de cargue]) [Fecha de cargue] 
				FROM StageCorporativa.stage.extrPlanoCallCenterCargados WHERE IndEncontrado = 0 GROUP BY CEDULA, Periodo,IndEncontrado)*/ O 
		on a.strIdentificacion = O.CEDULA and a.strPeriodo = o.Periodo --and IndEncontrado = 0
  WHERE BodegaCorporativa.$partition.pf_mes(dtmFechaInsercion) >= BodegaCorporativa.$partition.pf_mes('2022-01-01')
  --and e.strDescTipoRango = 'Fuente Leads'
) A 
/*inner join [StageCorporativa].[Stage].[extrPlanoCallCenterGestiones] b 
	on a.strIdentificacion = b.ID_CLIENTE 
	AND FECHA_GESTI�N BETWEEN cast(dtmfecharegistro AS date) AND CAST(DATEADD(m,3,dtmfecharegistro) as date) */
WHERE ordenFuente = 1 


SELECT [skTiempo]
      ,[strIdentificacion]
      ,[numAnio]
      ,[strDescMes]
      ,[skCampa�a]
      ,[strAgrupacionCampana]
      ,[strDescCampana]
      ,[strDescCampanaHomologado]
      ,[skRangoCanalLeads]
      ,[skRangoFuenteLeads]
      ,[FuenteLeads]
      ,[strDescCanalLeads]
      ,[indRegistroValido]
      ,[indVinculadoNuevo]
      ,[indRegistroDepurado]
      ,[IndCargadoCallCenter]
      ,[IndCalentamiento]
      ,[IndGestion]
      ,[IndContactado]
      ,[IndContactoDirecto]
      ,[indEfectivo]
      ,[indPreingresos]
      ,[indVinculadoNuevoSinCono]
      ,[indPreingresosSinCono]
      ,[dtmFechaVinculacion]
      ,[dtmFechaRegistro]
      ,[strTipoLead]
      ,[strCanalCallCenter]
      ,[dtmFechaGestionCallCenter]
      ,[dtmFechaCargueCallCenter]
      ,[Fecha de cargue]
      ,[dtmFechaCalentamiento]
      ,[strDescTipoCanalLeads]
      ,[strBaseCallCenter]
      ,[dtmFechaEfectivoCallCenter]
      ,[strBaseCallCenterEfectivo]
      ,[AgenteCallCenterEfectivo]
      ,[FuenteLeadsVisualizar]
      ,[strFuente]
      ,a.[strPeriodo]
      ,[strDescCargo]
      ,[strNombreAsesor]
      ,[strNombreJefe]
      ,[strDescTipoAsesor]
      ,[strDescCategoria]
      ,[strDescEstado]
      ,[strNombreGerenteZona]
      ,[CanalCallCenter]
      ,[DescMotivoNoContacto]
      ,[AgrupacionMotivoNoContacto]
      ,[DescMotivoNoEfectivo]
      ,[AgrupacionMotivoNoEfectivo]
      ,[DescMotivoGestionContanto]
      ,[AgrupacionMotivoGestionContanto]
      ,[DescMotivoEfectivo]
      ,[AgrupacionMotivoEfectivo]
      ,[strCanalCallCenterEfectivo]
      ,[DescMotivoPendienteContacto]
      ,[AgrupacionMotivoPendienteContacto]
      ,[ordenFuente]
      ,[skAsesor]
      ,[strDescCategoriaAgrupacion]
      ,[strDescCategoriaVisualizar]
      ,[CantidadInteracciones]
	  ---
	  [ID_LLAMADA]
      ,[ID_MUESTRA]
      ,[CAMPA�A REAL]
      ,[CAMPA�A]
      ,[NOMBRE_MUESTRA]
      ,[FECHA_CARGUE_MUESTRA]
      ,[ID_CODE]
      ,[CODE]
      ,[AGENTE]
      ,[ID_AGENTE]
      ,[NOMBRE_AGENTE]
      ,[ID_CLIENTE]
      ,[NOMBRE_CLIENTE]
      ,[CORREO_CLIENTE]
      ,[TELEFONO_CLIENTE]
      ,[FECHA_GESTI�N]
      ,[PREPARACION]
      ,[AGENTE_TIMBRA]
      ,[CUELGA_LLAMADA]
      ,[ESCRITURA]
      ,[OBSERVACIONES]
      ,[BASE]
      ,[TIPO]
      ,[GRUPO-TIPO]
      ,[HORA]
      ,[DIA]
      ,[NOMBRE_DIA]
      ,[SEMANA_MES]
      ,[PREVIEW]
      ,[PREVIEW_2]
      ,[ACD]
      ,[ACW]
      ,[TMO]
      ,[TMO_2]
      ,[CAMPA�A-CODE]
      ,[CONTACTOS]
      ,[CONTACTOS_DIRECTOS]
      ,[EFECTIVOS]
      ,b.[strPeriodo] [strPeriodoGestiones]
	INTO [Datamining].[datamining].[factLeads_2022_3meses_CompletaV2_DetalleGestiones]
	FROM [Datamining].[datamining].[factLeads_2022_3meses_CompletaV2] a
	INNER JOIN [StageCorporativa].[Stage].[extrPlanoCallCenterGestiones] B 
	on a.strIdentificacion = b.ID_CLIENTE 
	AND FECHA_GESTI�N BETWEEN cast(dtmfecharegistro AS date) AND CAST(DATEADD(m,3,dtmfecharegistro) as date) 

--Cantidad de interacciones
UPDATE A SET CantidadInteracciones =
--select *, 
(SELECT COUNT(1) 
				FROM [StageCorporativa].[Stage].[extrPlanoCallCenterGestiones] B WHERE strIdentificacion = b.ID_CLIENTE
						AND FECHA_GESTI�N BETWEEN cast(a.dtmfecharegistro AS date) AND CAST(DATEADD(m,3,a.dtmfecharegistro) as date)
				)
from [BodegaCorporativa].[bodega].[factLeads_2022_3meses_CompletaV2] a 

/*
select * from [BodegaCorporativa].[bodega].[factLeads_2022_3meses_Completa] a 
where strIdentificacion ='1110603970'

select count(1) FROM [Datamining].[datamining].[factLeads_2022_3meses_CompletaV2_DetalleGestiones]
*/


--and stridentificacion = '1110603970'
/*
SELECT skTiempo
	,strIdentificacion	
	,numAnio	
	,strDescMes	
	,skCampa�a	
	,strAgrupacionCampana	
	,strDescCampana	
	,strDescCampanaHomologado	
	,skRangoCanalLeads	
	,skRangoFuenteLeads	
	,FuenteLeads
	,strDescCanalLeads
	,COUNT(1) Cantidad
FROM #tmp
GROUP BY skTiempo
	,strIdentificacion	
	,numAnio	
	,strDescMes	
	,skCampa�a	
	,strAgrupacionCampana	
	,strDescCampana	
	,strDescCampanaHomologado	
	,skRangoCanalLeads	
	,skRangoFuenteLeads	
	,FuenteLeads
	,strDescCanalLeads
*/
--return 
/*select * from [StageCorporativa].[Stage].[extrPlanoCallCenterGestiones] b 
  where FECHA_GESTI�N BETWEEN cast('2022-04-12' AS date) AND DATEADD(m,3,'2022-04-12')
  and b.ID_CLIENTE ='1110603970'
*/

select COUNT(1) from [BodegaCorporativa].[bodega].[factLeads_2022_3meses_CompletaV2] where [IndCargadoCallCenter]=1

SELECT count(1) FROM [Datamining].[datamining].[factLeads_2022_3meses_CompletaV2_DetalleGestiones]